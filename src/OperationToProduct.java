/**

 * This class manages all functions relate to the product

 */

public class OperationToProduct {



    /**

     * Searching and returning the index of product p in the list. If not found

     * return -1.

     *

     * @param p    Product for searching

     * @param list The Linked List

     * @return The index of product p in the list

     */

//    public int index(Product p, MyList<Product> list) {
//
//    }



    /**

     * Creating and returning a product with info input from keyboard.

     *

     * @return The product

     */

//    public Product createProduct() {
//
//    }



    /**

     * Reading all products from the file and insert them to the list at tail.

     *

     * @param fileName The file name of the file

     * @param list     The Linked List contains all products that read from file

     */

    public void getAllItemsFromFile(String fileName, MyList<Product> list) {

    }



    /**

     * Reading all products from the file and insert them to the stack.

     *

     * @param fileName The file name of the file

     * @param stack     The Stack contains all products that read from file

     */

    public void getAllItemsFromFile(String fileName, MyStack<Product> stack) {

    }



    /**

     * Reading all products from the file and insert them to the queue.

     *

     * @param fileName The file name of the file

     * @param queue     The Queue contains all products that read from file

     */

    public void getAllItemsFromFile(String fileName, MyQueue<Product> queue) {

    }



    /**

     * Adding a product to the list, info of the product input from keyboard.

     *

     * @param list The Linked list

     */

    public void addLast(MyList<Product> list) {

    }



    /**

     * Printing all prodcuts of the list to console screen

     *

     * @param list

     */

    public void displayAll(MyList<Product> list) {

    }



    /**

     * Writing all products from the list to the file

     *

     * @param fileName Input file name

     * @param list     Input Linked list

     */

    public void writeAllItemsToFile(String fileName, MyList<Product> list) {

    }



    /**

     * Searching product by ID input from keyboard.

     *

     * @param list

     */

    public void searchByCode(MyList<Product> list) {

    }



    /**

     * Deleting first product that has the ID input from keyboard from the list.

     *

     * @param list

     */

    public void deleteByCode(MyList<Product> list) {



    }



    /**

     * Sorting products in linked list by ID

     *

     * @param list The Linked list

     */

    public void sortByCode(MyList<Product> list) {



    }



    /**

     * Adding new product to head of Linked List. The info input from keyboard.

     *

     * @param list The linked list

     */

    public void addFirst(MyList<Product> list) {



    }



    /**

     * Convert a decimal to a integer number. Example: input i = 18 -> Output = 10010

     * @param i Input decimal number

     * @return a integer numbers

     */

//    public int convertToBinary(int i) {
//
//    }
//


    /**

     * Deleting the product at position

     *

     * @param list The Linked List

     * @param pos  The position of product to be deleted

     */

    public void deleteAtPosition(MyList<Product> list, int pos) {



    }

}