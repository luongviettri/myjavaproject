import java.util.Scanner;

public class AS2_Main {
    private static Scanner scanner = new Scanner(System.in);
    MyList myList = new MyList();

    public static void showMenu() {
        int input = 0;
        boolean isValid = false;
        do {
            displayOptions();
            //! cho nta nhập sau đó valid
            input = handleValid(input, isValid);
            //! handle Option
            handleOption(input);
            System.out.println(input);
        } while (input != 0);
    }

    public static void displayOptions() {
        System.out.println("Choose one of this options:");
        System.out.println("Product list:");
        System.out.println("1. Load data from file and display");
        System.out.println("2. Input & add to the end.");
        System.out.println("3. Display data");
        System.out.println("4. Save product list to file.");
        System.out.println("5. Search by ID");
        System.out.println("6. Delete by ID");
        System.out.println("7. Sort by ID.");
        System.out.println("8. Convert to Binary");
        System.out.println("9. Load to stack and display");
        System.out.println("10. Load to queue and display.");
        System.out.println("0. Exit");
        System.out.print("-------Mời bạn chọn chức năng:... ");
    }

    public static void handleOption(int input) {
        switch (input) {
            case 1:
                System.out.println("some thing");
                break;
            case 2:
                System.out.println("some thing");
                break;
            case 3:
                System.out.println("some thing");
                break;
            case 4:
                System.out.println("some thing");
                break;
            case 5:
                System.out.println("some thing");
                break;
            case 6:
                System.out.println("some thing");
                break;
            case 7:
                System.out.println("some thing");
                break;
            case 8:
                System.out.println("some thing");
                break;
            case 9:
                System.out.println("some thing");
                break;
            case 10:
                System.out.println("some thing");
                break;
            case 0:
                System.out.println("some thing");
                break;
        }
    }

    public static int handleValid(int input, boolean isValid) {
        do {
            try {
                input = Integer.parseInt(scanner.nextLine());
                isValid = true;
                return input;
            } catch (Exception e) {
                System.out.println("Có gì đó sai sai, Xin nhập lại số:");
                isValid = false;
            }
        } while (!isValid);
        return 999;
    }

    public static void main(String[] args) {
        showMenu();
//        Product product = new Product("1","2",3, 10.0);
//        MyList myLinkedList = new MyList(product);
//        myLinkedList.printList();
    }

}