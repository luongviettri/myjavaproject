/**
 * Generic version of the LinkedList class.
 *
 * @param <T> the type of the value
 */

public class MyList<T> {
    /**
     * Head node, default is null
     */
   private Node<T> head;
    /**
     * Tail node, default is null
     */
   private Node<T> tail;
   private int length = 0;

    public Node<T> getHead() {
        return head;
    }
    public void setHead(Node<T> head) {
        this.head = head;
    }
    public Node<T> getTail() {
        return tail;
    }
    public void setTail(Node<T> tail) {
        this.tail = tail;
    }
    public int getLength() {
        return length;
    }
    public void setLength(int length) {
        this.length = length;
    }
    public MyList() {

    }
    /**
     * Constructor with head and tail
     *
     */
    public MyList(Product product) {
        Node<Product> newNode = new Node<>(product);
        head = (Node<T>) newNode;
        tail = (Node<T>) newNode;
        length = 1;
    }
    /**
     * Checking if this list is empty
     *
     * @return true if list is empty

     */
//    public boolean isEmpty() {
//
//    }
    /**
     * Returning the length of this list
     *
     * @return The length of this list

     */
//    public int length() {
//
//    }
//
//
    /**
     * Insert an item to the head of this list
     *
     * @param item The item to be inserted
     */
    public void insertToHead(T item) {

    }
    /**
     * Insert an item at position to this list
     *
     * @param position The position of new item
     * @param item     The item to be inserted
     */
    public void insertAfterPosition(int position, T item) {

    }
    /**
     * Deleting the tail of this list
     */
    public void deleteTail() {

    }
    /**
     * Searching and deleting an item from this list by comparing the ID of items
     *
     * @param item The item to be deleted
     */
    public void deleteElement(T item) {

    }
    /**
     * Swaping two nodes [firstNode] and [secondNode]
     *
     * @param firstNode
     * @param secondNode
     */
    public void swap(Node<T> firstNode, Node<T> secondNode) {

    }
    /**
     * Deleting all items in the list
     */
    public void clear() {

    }


    public void printList() {
        Node temp = head;
        while (temp != null) {
            System.out.println(temp.info);
            temp = temp.next;
        }
    }
}